


******

Bravo!  You have received a Medical Diploma from   
the Orbital Convergence University International Air 
and Water Embassy of the Tangerine Planet.  

You are now officially certified to include this
in your practice.

******

# metal_shredding_center

## Caution!  This is for deleting the contents of drives.
## Use cautiously.


## install
pip install metal_shredding_center

## check the drives
```
lsblk -b -p
fdisk -l
fdisk -l /dev/sdb
```


## utility
### delete
This does 2 loops, 
````
    loop 1:
        sculpt
        scan
    
    loop 2:
        sculpt
        scan
```

````
	metal_shredding_center_1 erase entirely --drive-path /dev/sdb --skip-over 3
```


It's possible to check with:
````
xxd -len 20 -seek 31914983420 /dev/sde
````

### delete, from iteration 2
````
metal_shredding_center erase entirely --skip-over 2 --drive-path /dev/sd0000
```

skip_over = 0 -> doesn't skip
skip_over = 1 -> skips sculpt 1
skip_over = 2 -> skips sculpt 2



### verifications
```
apt install xxd -7

lsblk -b /dev/sdb

xxd -len 64 /dev/sdb
```
```
00000000: ffef ffdf ffcf ffbf ffef ffdf ffcf ffbf  ................
00000010: ffef ffdf ffcf ffbf ffef ffdf ffcf ffbf  ................
00000020: ffef ffdf ffcf ffbf ffef ffdf ffcf ffbf  ................
00000030: ffef ffdf ffcf ffbf ffef ffdf ffcf ffbf  ................
```

```
xxd -len 20 -seek 40018599916 /dev/sdb
```